package tablecloth.server;

public abstract interface IMixinRealTimeTicking
{
  public abstract long getRealTimeTicks();
}
