package tablecloth.server.command;

import com.google.common.collect.Lists;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;

public class CommandPlugin extends Command {

    static {
        new CommandPlugin("plugin");
    }

    public CommandPlugin(String name) {
        super(name);
        this.description = "Load or unload plugin";
        this.usageMessage = "/plugin <load|unload> <name>";
        this.setPermission("tableclot.command.plugin");
    }

    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (!this.testPermission(sender)) {
            return true;
        } else if (args.length < 2) {
            sender.sendMessage(ChatColor.GOLD + "Usage: " + this.usageMessage);
            return false;
        } else {
            String action = args[0].toLowerCase();
            String pluginName = args[1];

            try {
                if (action.equals("unload")) {
                    this.unloadPlugin(pluginName, sender);
                } else if (action.equals("load")) {
                    this.loadPlugin(pluginName, sender);
                } else {
                    sender.sendMessage(ChatColor.GREEN + "Invalid action specified.");
                }
            } catch (Exception var7) {
                sender.sendMessage(ChatColor.GREEN + "Error with " + pluginName + ": " + var7.toString());
            }

            return true;
        }
    }

    public List tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        List tabs = Lists.newArrayList();
        if (args.length > 1) {
            String action = args[0].toLowerCase();
            int var7;
            int var8;
            if (action.equals("unload")) {
                Plugin[] var6 = Bukkit.getServer().getPluginManager().getPlugins();
                var7 = var6.length;

                for(var8 = 0; var8 < var7; ++var8) {
                    Plugin plugin = var6[var8];
                    tabs.add(plugin.getName());
                }
            } else if (action.equals("load")) {
                File[] var10 = (new File("plugins")).listFiles();
                var7 = var10.length;

                for(var8 = 0; var8 < var7; ++var8) {
                    File file = var10[var8];
                    if (file.isFile() && file.getName().toLowerCase().endsWith(".jar")) {
                        tabs.add(file.getName().substring(0, file.getName().length() - 4));
                    }
                }
            }
        }

        return tabs;
    }

    private void unloadPlugin(String pluginName, CommandSender sender) throws Exception {
        SimplePluginManager manager = (SimplePluginManager)Bukkit.getServer().getPluginManager();
        List plugins = (List)ReflectionHelper.getPrivateValue(SimplePluginManager.class, manager, new String[]{"plugins"});
        Map lookupNames = (Map)ReflectionHelper.getPrivateValue(SimplePluginManager.class, manager, new String[]{"lookupNames"});
        SimpleCommandMap commandMap = (SimpleCommandMap)ReflectionHelper.getPrivateValue(SimplePluginManager.class, manager, new String[]{"commandMap"});
        Map knownCommands = (Map)ReflectionHelper.getPrivateValue(SimpleCommandMap.class, commandMap, new String[]{"knownCommands"});
        Plugin[] var8 = manager.getPlugins();
        int var9 = var8.length;

        for(int var10 = 0; var10 < var9; ++var10) {
            Plugin plugin = var8[var10];
            if (plugin.getDescription().getName().equalsIgnoreCase(pluginName)) {
                manager.disablePlugin(plugin);
                plugins.remove(plugin);
                lookupNames.remove(pluginName);
                Iterator it = knownCommands.entrySet().iterator();

                while(it.hasNext()) {
                    Entry entry = (Entry)it.next();
                    if (entry.getValue() instanceof PluginCommand) {
                        PluginCommand command = (PluginCommand)entry.getValue();
                        if (command.getPlugin() == plugin) {
                            command.unregister(commandMap);
                            it.remove();
                        }
                    }
                }

                sender.sendMessage(ChatColor.GREEN + "Unloaded " + pluginName + " successfully!");
                return;
            }
        }

        sender.sendMessage(ChatColor.GREEN + "Can't found loaded plugin: " + pluginName);
    }

    private void loadPlugin(String pluginName, CommandSender sender) {
        try {
            PluginManager manager = Bukkit.getServer().getPluginManager();
            File pluginFile = new File("plugins", pluginName + ".jar");
            if (!pluginFile.exists() || !pluginFile.isFile()) {
                sender.sendMessage(ChatColor.GREEN + "Error loading " + pluginName + ".jar, no plugin with that name was found.");
                return;
            }

            Plugin plugin = manager.loadPlugin(pluginFile);
            plugin.onLoad();
            manager.enablePlugin(plugin);
            sender.sendMessage(ChatColor.GREEN + "Loaded " + pluginName + " successfully!");
        } catch (Exception var6) {
            sender.sendMessage(ChatColor.GREEN + "Error loading " + pluginName + ".jar, this plugin must be reloaded by restarting the server.");
        }

    }
}

