package tablecloth.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class DownloadLibraries {

    public DownloadLibraries() {
        String url = "https://ckateptb.github.io/java/minecraft/Tablecloth/libraries.zip";
        String fileName = "libraries.zip";
        download(url, fileName);
        try {
            @SuppressWarnings("resource")
            ZipFile zip = new ZipFile(new File(fileName), Charset.forName("GBK"));
            File filePath = new File(".");
            if (!(filePath.exists())) {
                filePath.mkdirs();
            }
            System.out.println("Unpacking " + fileName);
            for (Enumeration<? extends ZipEntry> entries = zip.entries(); entries.hasMoreElements(); ) {
                ZipEntry entry = entries.nextElement();
                String zipEntryName = entry.getName();
                InputStream is = zip.getInputStream(entry);
                String outPath = ("." + "/" + zipEntryName).replaceAll("\\*", "/");
                File file1 = new File(outPath.substring(0, outPath.lastIndexOf('/')));
                if (!(file1.exists())) {
                    file1.mkdirs();
                }
                if (new File(outPath).isDirectory()) {
                    continue;
                }
                FileOutputStream fos = new FileOutputStream(outPath);
                byte[] b = new byte[1024];
                int i;

                while ((i = is.read(b)) > 0) {
                    fos.write(b, 0, i);
                }
                is.close();
                fos.close();
            }
            System.out.println("DONE!");
        } catch (IOException e) {
            System.out.println(fileName + "unzip failed!");
            System.exit(1);
        }
    }

    private void download(String url, String fileName) {
        System.out.println("Downloading " + fileName);
        try {
            URL website = new URL(url);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            System.out.println("DONE!");
        } catch (IOException e) {
            System.out.println(fileName + "download failed!\nCheck your internet connection.");
            System.exit(1);
        }
    }

}
