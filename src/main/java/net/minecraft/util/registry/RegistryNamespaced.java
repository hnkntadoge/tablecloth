package net.minecraft.util.registry;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.IObjectIntIterable;
import net.minecraft.util.IntIdentityHashBiMap;
import org.bukkit.Material;
import tablecloth.api.ServerAPI;

import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.Map;

public class RegistryNamespaced<K, V> extends RegistrySimple<K, V> implements IObjectIntIterable<V> {
    protected final IntIdentityHashBiMap<V> underlyingIntegerMap = new IntIdentityHashBiMap<V>(256);
    protected final Map<V, K> inverseObjectRegistry;

    public RegistryNamespaced() {
        this.inverseObjectRegistry = ((BiMap) this.registryObjects).inverse();
    }

    public void register(int id, K key, V value) {
        // Cauldron start - register item/block materials for Bukkit
        boolean isForgeBlock = value instanceof Block && (value.getClass().getName().length() > 3 && !value.getClass().getName().startsWith("net.minecraft.block")) ? true : false;
        String materialName = Material.normalizeName(key.toString());
        org.bukkit.Material material;
        if (isForgeBlock) {
            material = org.bukkit.Material.addMaterial(id, materialName, true);
        } else {
            material = org.bukkit.Material.addMaterial(id, ((Item) value).getItemStackLimit(), materialName);
        }
        if (material != null) {
            if (isForgeBlock) {
                ServerAPI.injectblock.put(material.name(), material.getId());
            } else {
                ServerAPI.injectmaterials.put(material.name(), material.getId());
            }
        }
        // Cauldron end
        this.underlyingIntegerMap.put(value, id);
        this.putObject(key, value);
    }

    protected Map<K, V> createUnderlyingMap() {
        return HashBiMap.<K, V>create();
    }

    @Nullable
    public V getObject(@Nullable K name) {
        return (V) super.getObject(name);
    }

    @Nullable
    public K getNameForObject(V value) {
        return this.inverseObjectRegistry.get(value);
    }

    public boolean containsKey(K key) {
        return super.containsKey(key);
    }

    public int getIDForObject(@Nullable V value) {
        return this.underlyingIntegerMap.getId(value);
    }

    @Nullable
    public V getObjectById(int id) {
        return this.underlyingIntegerMap.get(id);
    }

    public Iterator<V> iterator() {
        return this.underlyingIntegerMap.iterator();
    }
}